# README #

Bienvenue dans ce petit projet de test consistant à ajouter un système de notation 

### A savoir ###

Les données de test sont générées par Foundry et via les Fixtures. Cela devrait se faire automatiquement.

* URL du docker : http://127.0.0.1:8741/
* Identification ici : http://127.0.0.1:8741/login/
* Utilisateur admin :
    * login: cheesefan@example.com
    * pass: admin


### Dossiers principaux ###

```
project/public/css/*
project/public/js/*
project/templates/base.html.twig
project/templates/pages/product.html.twig
```

### Initialiser la bdd si besoin ###

```
php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

### TODO ###

* Gérer toutes les erreurs de formulaire
* Installer et mettre en place SASS
* Récupérer Bootstrap
* Récupérer la base CSS et HTML d'izidore
* Optimiser
* Limiter le nombre de requêtes