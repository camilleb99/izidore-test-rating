// TODO: rewrite

var StarRating = {

	init: function() {

		var $star_rating_container = $('#rating_product_rating');

		$star_rating_container.each(function() {

			var
				$rating_form_entry = $(this),
				$rating_container = $('<div class="jsstarrating"></div>'),
				rating_container_original_classes = $rating_container.attr('class');

			$(this).find('option').each(function() {

				var
					$current_option = $(this),
					$new_rating_entry = $('<span>'+'</span>');

				$new_rating_entry.attr('title', $current_option.text());
				
				$new_rating_entry.on('click', function() {
					$rating_form_entry.val($current_option.val());
					$rating_container.attr('class', rating_container_original_classes);
					$rating_container.addClass('rating-'+$current_option.val());
				});

				$rating_container.append($new_rating_entry);

			});

			$(this).before($rating_container);

			$rating_form_entry.addClass('sr-only');

		})

	}

};
