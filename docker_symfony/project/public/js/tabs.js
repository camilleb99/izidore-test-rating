var Tabs = {

	vars: {
		$container: $('.tabs-container'),
		$buttonsContainer: $('.tabs-container .tab-buttons'),
		$buttons: $('.tabs-container .tab-buttons a')
	},

	disableAllTabs: function() {
		Tabs.vars.$buttonsContainer.find('.active').removeClass('active');
		Tabs.vars.$container.find('.tab-content.active').removeClass('active');
	},

	activateTab: function($button, target) {
		$button.addClass('active');
		$(target).addClass('active');
	},

	init: function() {

		Tabs.vars.$buttons.on('click', function(e) {

			e.preventDefault();

			var
				$button = $(this),
				target = $button.attr('href');

			Tabs.disableAllTabs();
			Tabs.activateTab($button, target);

		})

	}

};
